module main_function_tb();
    reg clk;
    reg rst;
    reg [7:0] a;
    reg [7:0] b;
    reg start;
    wire busy;
    wire [15:0] result;
    main_function main1 (
        .clk_i(clk),
        .rst_i(rst),
        .a_bi(a),
        .b_bi(b),
        .start_i(start),
        .busy_o(busy),
        .result_bo(result)
    );
    initial begin
        clk=0;rst=1; a=8'd8; b=8'd121; start = 0;
        #2
        rst=0;
        start=1;
        #2
        start=0;
        #100
         a=8'd121; b=8'd9;
         #2;
         start = 1;
        #2
        start = 0;
        #100
         a=8'd64; b=8'd16;
         #2;
         start = 1;
        #2
        start = 0;
        #100
         a=8'd54; b=8'd81;
         #2;
         start = 1;
         #2
         start = 0;
        end
    
    always #1 clk = ~clk;
endmodule