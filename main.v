`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03/17/2021 08:36:49 PM
// Design Name: 
// Module Name: function
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module main_function(
    input clk_i,    
    input [7:0] a_bi,
    input [7:0] b_bi,
    input start_i,
    input rst_i,
    output reg [15:0] result_bo,
    output busy_o
);
    localparam IDLE = 2'b00;
    localparam WORK_SQRT = 2'b01; 
    localparam WORK_MULT = 2'b10;
    reg [7:0] a;
    reg [7:0] b;
	reg [7:0] mult_b;
    reg [1:0] state;
    reg start_mult;
    reg start_sqrt;
    wire sqrt_busy;
    wire mult_busy;
    wire [15:0] mult_result;
    wire [3:0] sqrt_result;
    mult multi(
        .clk_i(clk_i),
        .rst_i(rst_i),
        .a_bi(a),
		.b_bi(mult_b),
        .start_i(start_mult),
        .busy_o(mult_busy),
        .y_bo(mult_result)
    );
    sqrt sqrt1 (
        .clk_i(clk_i),
        .rst_i(rst_i),
        .start_i(start_sqrt),
        .x_bi(b),
        .busy_o(sqrt_busy),
        .y_bo(sqrt_result)
    );
    assign busy_o = state != IDLE;
    always @(posedge clk_i) begin
        if(rst_i) begin
            state = IDLE;
            result_bo = 0;
        end else begin
            case(state)
                IDLE:
                    if(start_i) begin
                        a = a_bi;
                        b = b_bi;
                        state = WORK_SQRT;
                        start_sqrt = 1;
                    end
                WORK_SQRT:
                    begin
                        start_sqrt = 0;    
						if(sqrt_busy == 0)
						begin
						    state = WORK_MULT;
    						mult_b = sqrt_result;
							start_mult = 1;
						end
	               end
	           WORK_MULT:
	               begin
	                    start_mult = 0;    
						if(mult_busy == 0)
						begin
    						state = IDLE;
                            result_bo = mult_result;
						end
	               end
            endcase
        end
    end
    
endmodule