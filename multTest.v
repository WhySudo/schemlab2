`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 28.03.2021 03:17:58
// Design Name: 
// Module Name: SimTest2
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module SimTest2(

    );
   reg clk_i;
   reg rst_i;
   reg [ 7 : 0 ] a_bi;
   reg [ 7 : 0 ] b_bi;
   reg start_i;
   wire busy_o;
   wire [ 15 : 0 ] y_bo;
   
   
   Mult mult1(
   .clk_i(clk_i),
   .rst_i(rst_i),
   .a_bi(a_bi),
   .b_bi(b_bi),
   .start_i(start_i),
   .busy_o(busy_o),
   .y_bo(y_bo)
   );
   
   integer i;
   integer test_val ;
   reg expected_val ;
   
   initial 
   begin
   for ( test_val = 0 ; test_val < 4 ; test_val = test_val+1) 
   begin
   #10 clk_i = 1; rst_i=1; 
   #10 clk_i = 0; rst_i=0;
   #10
    a_bi = test_val[0];
    b_bi = test_val[1];
    clk_i = 1;
    start_i=1;
    #10 clk_i = 0; 

#10 clk_i = 1;start_i = 0;
    #10 clk_i = 0; #10 clk_i = 1;  
    #10 clk_i = 0; #10 clk_i = 1;
    #10 clk_i = 0; #10 clk_i = 1;  
    #10 clk_i = 0; #10 clk_i = 1;
    #10 clk_i = 0; #10 clk_i = 1;
    #10 clk_i = 0; #10 clk_i = 1;  
    #10 clk_i = 0; #10 clk_i = 1;   
    #10 clk_i = 0;
    #10
        
    expected_val =a_bi * b_bi;
    if(y_bo == expected_val) 
    begin
        $display ( "correct a_bi=%b ,b_bi=%b y_bo = %b" , a_bi , b_bi , y_bo ) ;
    end 
    else 
    begin
        $display ( "wrong a_bi=%b , b_bi=%b y_bo=%b , expected %b" , a_bi , b_bi ,y_bo , expected_val );
    end
    
    end
end
//#10 $stop ;

endmodule